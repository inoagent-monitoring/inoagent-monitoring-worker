import logging
from pathlib import Path

import sentry_sdk
from environ import Env
from sentry_sdk.integrations.logging import LoggingIntegration

env = Env()
env.read_env()

__all__ = [
    "VERSION",
    "BASE_DIR",
    "DATABASE_DSN",
    "DEBUG",
    "USE_TZ",
    "SENTRY_DSN",
]

VERSION = env.str("VERSION", default="1.0.0")
TAG = env.str("TAG", default="local")
BASE_DIR = Path(__file__).resolve().parent.parent
DATABASE_DSN = env.str(
    var="DATABASE_DSN",
    default="postgresql://postgres:12345@localhost:5432/inoagent-monitoring-worker",
)
DEBUG = env.bool("DEBUG", default=False)
USE_TZ = env.bool("USE_TZ", default=True)

if SENTRY_DSN := env.str("SENTRY_DSN", default=""):
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        release=VERSION,
        environment=TAG,
        traces_sample_rate=1.0,
        send_default_pii=True,
        attach_stacktrace=True,
        profiles_sample_rate=1.0,
        enable_tracing=True,
        integrations=[
            LoggingIntegration(
                level=logging.INFO,  # Capture info and above as breadcrumbs
                event_level=logging.ERROR,  # Send errors as events
            ),
        ]
    )
