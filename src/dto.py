from collections.abc import Iterable
from datetime import date
from typing import NamedTuple

__all__ = [
    "InoAgentInformationDto",
]


class InoAgentInformationDto(NamedTuple):
    # Полное наименование/ФИО
    name: str  # 1
    # Основание включения
    grounds_for_inclusion: str  # 10
    # Дата рождения
    birthday: date | None = None  # 2
    # Адрес (место рождения)
    address: str | None = None  # 7
    # Полное наименование/ФИО участников
    full_name: Iterable[str] | None = None  # 9
    # ИНН
    inn: str | None = None  # 4
    # ОГРН
    ogrn: str | None = None  # 3
    # Дата принятия решения о включении
    decision_inclusion_date: date | None = None  # 11
    # Дата принятия решения об исключении
    decision_exclusion_date: date | None = None  # 12
    # Информационный ресурс
    information_resource: Iterable[str] = None  # 8
    # Регистрационный номер
    registration_number: str | None = None  # 5
    # СНИЛС
    snils: str | None = None  # 6
