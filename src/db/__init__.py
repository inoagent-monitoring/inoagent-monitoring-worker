from sqlalchemy import event

from .session import Session  # noqa


def send_message_into_broker(session: Session) -> None:
    """Отправка события об изменении в брокере сообщений"""
    a = 5
    raise NotImplementedError


def increment_obj_version_number(session: Session) -> None:
    """Отправка события об изменении в брокере сообщений"""
    a = 5
    # TODO увеличить счетчик version + 1
    raise NotImplementedError


# TODO повесить "триггеры"
event.listen(Session, "after_commit", send_message_into_broker)
event.listen(Session, "before_commit", increment_obj_version_number)
