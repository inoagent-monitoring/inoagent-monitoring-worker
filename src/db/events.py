from sqlalchemy import event

event.listen(Session, "before_commit", my_before_commit)


class SendToBrokerAfterSave(MapperExtension):

    def _capitalize_name(self, instance):
        if instance.name is not None:
            instance.name = instance.name.capitalize()

    def before_insert(self, mapper, connection, instance):
        self._capitalize_name(instance)

    def before_update(self, mapper, connection, instance):
        self._capitalize_name(instance)
