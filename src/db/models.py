from datetime import datetime
from uuid import UUID, uuid4

from sqlalchemy import DateTime
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column
from sqlalchemy.sql import text, func

from src.conf import settings

__all__ = [
    "BaseModel",
]


class BaseModel(DeclarativeBase):
    """Абстрактная базовая модель."""
    __tablename__ = None

    pk: Mapped[int] = mapped_column(
        primary_key=True,
        autoincrement=True,
        index=True,
        comment="Первичный ключ",
    )
    uuid: Mapped[UUID] = mapped_column(
        default=uuid4,
        comment="Уникальный идентификатор",
    )
    is_active: Mapped[bool] = mapped_column(
        default=True,
        comment="Активный",
    )
    created_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=settings.USE_TZ),
        server_default=func.now(),
        comment="Дата создания",
    )
    updated_at: Mapped[datetime] = mapped_column(
        insert_default=func.now(),
        comment="Дата обновления",
        nullable=True,
    )

    version: Mapped[int] = mapped_column(default=0, server_default=text('0'))

    def __repr__(self) -> str:
        return f"{self.pk}"

    def __str__(self) -> str:
        return f"{self.pk}"
