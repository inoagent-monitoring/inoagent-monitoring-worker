import re
from collections.abc import Iterable
from datetime import date, datetime
from io import BytesIO

import httpx
from bs4 import BeautifulSoup, Tag
from pdfplumber.pdf import PDF
from pdfplumber.pdf import Page
from pydash.strings import clean

from .dto import InoAgentInformationDto
from .exceptions import RegistryFileNotFoundException

__all__ = [
    "parse_actual_inoagent_registry_file_page",
    "get_ino_agent_registry_file_by_search_date",
    "get_actual_inoagent_registry_file_date",
]


def parse_actual_inoagent_registry_file_page(page: Page) -> Iterable[InoAgentInformationDto]:
    """Parse actual inoagent registry file page."""
    rows = iter(page.extract_table())
    # skip table header
    next(rows)
    for row in rows:
        (
            _,
            name,
            birthday,
            ogrn,
            inn,
            registration_number,
            snils,
            address,
            information_resource,
            full_name,
            grounds_for_inclusion,
            decision_inclusion_date,
            decision_exclusion_date,
        ) = row

        if birthday:
            birthday = datetime.strptime(birthday, "%d.%m.%Y")
        if full_name:
            full_name = [
                clean(word)
                for word in clean(full_name.strip()).split(',')
            ]
        if information_resource:
            information_resource = [
                clean(resource)
                for resource in clean(information_resource.strip()).split(";")
            ]
        if decision_inclusion_date:
            decision_inclusion_date = datetime.strptime(decision_inclusion_date, "%d.%m.%Y")
        if decision_exclusion_date:
            decision_exclusion_date = datetime.strptime(decision_exclusion_date, "%d.%m.%Y")

        yield InoAgentInformationDto(
            name=clean(name),
            birthday=birthday or None,
            ogrn=ogrn or None,
            inn=inn or None,
            registration_number=registration_number and clean(registration_number) or None,
            snils=snils and clean(snils) or None,
            address=address and clean(address) or None,
            information_resource=information_resource or None,
            full_name=full_name or None,
            grounds_for_inclusion=grounds_for_inclusion and clean(grounds_for_inclusion) or None,
            decision_inclusion_date=decision_inclusion_date or None,
            decision_exclusion_date=decision_exclusion_date or None,
        )


def _download_actual_inoagent_registry_file_by_search_date(search_date: date | None = None) -> BytesIO:
    """Get inoagent registry file by search date or current date."""
    if search_date is None:
        search_date = datetime.now()

    search_date = search_date.strftime("%d%m%Y")
    resp = httpx.get(
        url=f"https://minjust.gov.ru/uploaded/files/reestr-inostrannyih-agentov-{search_date}.pdf",
        verify=False,
    )
    if resp.status_code == 404:
        raise RegistryFileNotFoundException(f"Registry file was not found on the requested date {search_date}")
    return BytesIO(resp.content)


def get_actual_inoagent_registry_file_date() -> date:
    """Получение даты актуального реестра иноагентов."""
    resp = httpx.get(
        url="https://minjust.gov.ru/ru/activity/directions/998/",
        verify=False,
    )
    if resp.status_code != 200:
        raise ValueError
    soup = BeautifulSoup(resp.content, "lxml")
    tag: None | Tag = soup.select_one("#section-description > div > ul > li:nth-child(2) > a")
    if tag is None:
        raise ValueError
    return datetime.strptime(re.search(r"\d{8}", tag.get("href")).group(0), "%d%m%Y").date()


def get_ino_agent_registry_file_by_search_date(search_date: date | None = None) -> Iterable[InoAgentInformationDto]:
    """Получение реестра иноагентов на определенную дату."""
    if search_date is None:
        search_date = datetime.now().date()

    registry_file = _download_actual_inoagent_registry_file_by_search_date(search_date=search_date)
    pdf = PDF(registry_file)
    for page in pdf.pages:
        yield from parse_actual_inoagent_registry_file_page(page=page)
