from sqlalchemy import Integer
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Mapped, mapped_column

from src.db.models import BaseModel

__all__ = [
    "Changelog",
]


class Changelog(BaseModel):
    """История изменений сущностей."""

    __tablename__ = "inoagent_changelog"

    # region старые данные
    old_data: Mapped[None | dict] = mapped_column(
        JSONB(),
        nullable=True,
        comment="Старые данные",
    )
    old_version: Mapped[None | int] = mapped_column(
        Integer(),
        comment="Номер старой версии",
    )
    # endregion

    # region  новые данные
    new_data: Mapped[None | dict] = mapped_column(
        JSONB(),
        nullable=False,
        comment="Новые данные",
    )
    new_version: Mapped[None | int] = mapped_column(
        Integer(),
        comment="Номер старой версии",
    )
    # endregion

    model_name: Mapped[str] = mapped_column(
        comment="Наименование модели",
        index=True,
    )
    table_name: Mapped[str] = mapped_column(
        comment="Наименование таблицы",
        index=True,
    )
    object_id: Mapped[int] = mapped_column(
        Integer(),
        comment="ID объекта",
    )

    # endregion

    def __repr__(self) -> str:
        return f"<{self.pk}> {self.table_name}: {self.model_name} - {self.object_id}"

    def __str__(self) -> str:
        return f"{self.table_name}: {self.model_name} - {self.object_id}"
