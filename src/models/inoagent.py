from datetime import date

from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Mapped, mapped_column

from src.db.models import BaseModel

__all__ = [
    "InoAgent",
]


class InoAgent(BaseModel):
    """Сведение об иноагенте."""

    __tablename__ = 'inoagent_inoagent_information'

    name: Mapped[str] = mapped_column(
        unique=True,
        nullable=False,
        index=True,
        comment="Наименование",
    )
    grounds_for_inclusion: Mapped[str] = mapped_column(
        nullable=False,
        index=True,
        comment="Основание включения",
    )
    birthday: Mapped[None | date] = mapped_column(
        nullable=True,
        comment="Дата рождения",
    )
    address: Mapped[None | date] = mapped_column(
        nullable=True,
        comment="Адрес (место рождения)",
    )
    full_name: Mapped[None | list] = mapped_column(
        JSONB(),
        nullable=True,
        comment="Полное наименование/ФИО участников",
    )
    inn: Mapped[None | str] = mapped_column(
        nullable=True,
        index=True,
        comment="ИНН",
    )
    ogrn: Mapped[None | str] = mapped_column(
        nullable=True,
        index=True,
        comment="ОГРН",
    )
    decision_inclusion_date: Mapped[None | date] = mapped_column(
        nullable=True,
        comment="Дата принятия решения о включении",
    )
    decision_exclusion_date: Mapped[None | date] = mapped_column(
        nullable=True,
        comment="Дата принятия решения об исключении",
    )
    information_resource: Mapped[None | list] = mapped_column(
        JSONB(),
        nullable=True,
        comment="Информационный ресурс",
    )
    registration_number: Mapped[None | str] = mapped_column(
        nullable=True,
        index=True,
        comment="Регистрационный номер",
    )
    snils: Mapped[None | str] = mapped_column(
        nullable=True,
        index=True,
        comment="СНИЛС",
    )

    def __repr__(self) -> str:
        return f"<{self.pk}> {self.name}"

    def __str__(self) -> str:
        return str(self.name)
